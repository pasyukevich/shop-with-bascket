import produce from 'immer';
import { ApplicationState, ApplicationAction } from './types';
import { LOAD_PRODUCTS_ERROR, LOAD_PRODUCTS_REQUEST, LOAD_PRODUCTS_SUCCESS } from './constants';

export const initialState: ApplicationState = {
    loading: {
        products: false,
    },
    products: [],
}

const rootReducer = (state = initialState, action: ApplicationAction): ApplicationState => {
    switch (action.type) {
        case LOAD_PRODUCTS_REQUEST:
            return produce(state, draft => {
                draft.loading.products = true;
            });
        case LOAD_PRODUCTS_SUCCESS:
            return produce(state, draft => {
                draft.loading.products = false;
                draft.products = action.products;
            });
        case LOAD_PRODUCTS_ERROR:
            return produce(state, draft => {
                draft.loading.products = false;
            });
    }
}

export default rootReducer;
