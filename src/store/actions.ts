import { LoadProductsRequest, LoadProductsError, LoadProductsSuccess, Product } from './types';
import { LOAD_PRODUCTS_ERROR, LOAD_PRODUCTS_REQUEST, LOAD_PRODUCTS_SUCCESS } from './constants';


export const loadProductsRequest = (): LoadProductsRequest => ({
    type: LOAD_PRODUCTS_REQUEST,
});

export const loadProcessesSuccess = (products: Product[]): LoadProductsSuccess => ({
    type: LOAD_PRODUCTS_SUCCESS,
    products
});

export const loadJobsError = (): LoadProductsError => ({
    type: LOAD_PRODUCTS_ERROR,
});
