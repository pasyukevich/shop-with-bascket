import { Action } from 'redux';
import { LOAD_PRODUCTS_ERROR, LOAD_PRODUCTS_REQUEST, LOAD_PRODUCTS_SUCCESS } from './constants';

export interface Product {
    id: number;
    name: string;
    cost: number;
    imported: boolean;
}

export interface LoadingState {
    products: boolean;
}

export interface ApplicationState {
    loading: LoadingState;
    products: Product[];
}

export interface LoadProductsRequest extends Action {
    type: typeof LOAD_PRODUCTS_REQUEST;
}

export interface LoadProductsSuccess extends Action {
    type: typeof LOAD_PRODUCTS_SUCCESS;
    products: Product[];
}

export interface LoadProductsError extends Action {
    type: typeof LOAD_PRODUCTS_ERROR;
}

export type ApplicationAction =
    | LoadProductsRequest
    | LoadProductsSuccess
    | LoadProductsError;
