import React, { FunctionComponent } from "react";
import styled from "styled-components";
import noop from "lodash/noop";
import { ProductItem } from "./components";
import { Product } from "../../store/types";

const sortedItems: Product[] = [
  {
    name: "chocklate",
    id: 1,
    cost: 1231,
    imported: false,
  },
  {
    name: "chocklate",
    id: 2,
    cost: 1231,
    imported: false,
  },
];

export const ProductList: FunctionComponent = () => {
  return (
    <StyledProductList>
      {sortedItems.map((item) => (
        <ProductItem handleClick={noop} key={item.id} {...item} />
      ))}
    </StyledProductList>
  );
};

const StyledProductList = styled.ul`
  padding: 0;
  margin-bottom: 30px;
`;
