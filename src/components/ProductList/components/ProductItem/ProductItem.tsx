import React, { FunctionComponent } from 'react';
import styled from 'styled-components';

export type ProductItemProps = {
    id: number,
    name: string,
    handleClick: any,
    cost: number,
    imported: boolean
};

export const ProductItem: FunctionComponent<ProductItemProps> = ({ id, name, handleClick, cost, imported }) => {
    
    return (
        <StyledProductItem>
                <StyledProductWrapper>
                   {name}
                </StyledProductWrapper>
        </StyledProductItem>
    );
};

const StyledProductItem = styled.li`
    position: relative;
    list-style-type: none;
    display: block;
    margin: 10px 0;
    background: #e0e8f5;
    border-radius: 3px;
    padding-left: 38px;
    padding-top: 12px;
    padding-bottom: 12px;
    padding-right: 49px;
    overflow: hidden;
`;

const StyledProductWrapper = styled.div`
    display: flex;
`;
