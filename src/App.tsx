import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import { BrowserRouter, Switch, Route, NavLink } from 'react-router-dom';
import { ProductList } from './components/ProductList';
import './App.css';

const App: FunctionComponent = () => {
    return (
        <BrowserRouter>
            <div>
                <nav>
                    Navigation:
                    <NavLink exact to="/products">
                        Products
                    </NavLink>
                </nav>
                <main>
                    <Wrapper>
                        <Switch>
                            <Route exact path="/products" component={ProductList} />
                            <Route component={ProductList} />
                        </Switch>
                    </Wrapper>
                </main>
            </div>
        </BrowserRouter>
    );
};

const Wrapper = styled.div`
    display: flex;
    justify-content: center;
`;

export default App;
